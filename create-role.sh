#!/bin/sh
# create a new role folder structure
NAME=$1
if [ -z "$NAME" ]; then
	NAME="undefined"
fi

mkdir -p roles/"$NAME"/tasks
mkdir -p roles/"$NAME"/handlers
mkdir -p roles/"$NAME"/templates
mkdir -p roles/"$NAME"/files
mkdir -p roles/"$NAME"/vars
mkdir -p roles/"$NAME"/defaults
mkdir -p roles/"$NAME"/meta
