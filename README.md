# infrastruktur-ansible

Definition der Ansible Playbooks um meine Infrastruktur zu unterhalten.

### Überblick

Die Infrastruktur setzt sich aus verschiedenen Servern zusammen. Es gibt Speicher-Server und Application-Server. Ausserdem gibt es eine Art Root-Server. Diese können virtuelle Maschinen starten.
Von den Speicher-Servern gibt es zwei Sorten. Zum einen ganz klassisch NAS mit NFS und WebDAV (und evtl rsync), zum anderen Backup-Server die nur eine SSH Schnittstelle zur Verfügung stellen und sich periodisch die Updates von den anderen NAS holen.
Bei den Application-Servern gibt es zwei unterstütze Architekturen. Zum einen ARM für Raspberry-Pi Systeme, zum anderen x86.

### Grundlegende Funktion

Es ist möglich über ein Ansible Inventory zu definieren welche Server welche Aufgaben im Netzwerk haben. MitHilfe von Ansible werden sie entsprechend ausgestattet. Wenn ein benötigter Host nicht auffindbar ist, wird er auf einem Root-Server mit Vagrant instantiert.
Es gibt ein zweistufiges Test-Konzept. Es werden die Server Varianten bei jedem Update geprüft. Als zweites Level steht ein Application-Server für Tests zur Verfügung.

### Speicherkonzept

Die Application-Server haben im Grunde keinen Daten ausser den Containern die auf ihnen laufen. Die Container (z.B. ein CalDAV oder IMAP Server) greifen auf ein NAS zurück um ihre Daten abzulegen.
Das NAS bietet nur eingeschränkte Schnittstellen. Alle erweiterten Dienste sollen über die Container abgedeckt werden. Benutzer können direkt auf das NAS zugreifen, wenn sie WebDAV oder NFS benutzen. Backups werden auf das NAS gepusht.
Die Backup-Server bieten keine Schnittstelle nach aussen. Backups werden in den Server gezogen. Bei Wiederherstellung, muss man sich auf den Server per SSH einloggen und einen push ankicken. Begründung: Verschlüssellungstrojaner und andere Angreifer können selbst mit den ausgibigsten Rechten keinen Schaden an den Backups anrichten.

### Application-Server

Die Application-Server starten Docker container.
 ==> Offener Punkt: wer definiert welche Container auf dem System gestartet werden (Scenario: Neu-Aufbau)

ä## Testkonzept

Die Infrastruktur ist auf Arch-Linux ausgelegt. Das erfordert eine sehr gute Testabdeckung um automatische Updates in dem Rolling-Release Umfeld zu ermöglichen.
Darum werden zwei Test-Level umgesetzt.
1) Test der Server-Installation. Es gibt Test-Instanzen für Root-, Application-ARM-, Application-x86-, Speicher- und Backup-Server. Diese installieren periodisch Aktualisierungen der Pakete und führen Test durch um die korrekte Funktion zu gewährleisten. Die Tests können zum Teil trivial sein, z.B. ob Docker container immer noch starten und eine Netzwerkverbindung aufbauen können.
2) Test der Container. Container müssen in zwei Situationen getestet werden. Zum einen wenn sie aktualisiert werden und zum Anderen wenn es neue Container sind. Für diese Tests steht ein produktiver Application Server zur Verfügung.

### Updatekonzept

Die Produktivsysteme haben keinen Zugriff auf die Arch-Repos. Stattdessen greifen sie auf ein internes Repository zu. Dort befinden sich nur verifizierte Pakete.
Die Test-Systeme benutzen ebenfalls ein internes Repository. Dieses ist allerdings als `Cache` konfiguriert und wird mit den Arch-Repos sychronisiert. Wenn die Tests aller Test-Systeme positiv ausfallen wird der Cache in das Produktiv-Repository kopiert. Dann stehen den Produktivsystemen die neuen Versionen zur Verfügung.

Dieses Verfahren erzwingt, dass neue Pakete immer auf den Test-Systemen installiert werden.

## Inventar

root-application-x86.hq.rudis-infrastruktur.de
root-application-arm.hq.rudis-infrastruktur.de
root-storage.hq.rudis-infrastruktur.de

application-x86-p.hq.rudis-infrastruktur.de
test-application-x86-p.hq.rudis-infrastruktur.de
application-x86-q.hq.rudis-infrastruktur.de
application-arm-p.hq.rudis-infrastruktur.de
application-arm-q.hq.rudis-infrastruktur.de
storage-p.hq.rudis-infrastruktur.de
storage-q.hq.rudis-infrastruktur.de
backup-p.hq.rudis-infrastruktur.de
backup-q.hq.rudis-infrastruktur.de
