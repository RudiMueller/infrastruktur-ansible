#!/bin/sh
# install python on a host using Ansible

HOST=$1
if [ -z "$HOST" ]; then
	HOST="localhost"
fi
ansible "$HOST" -b --become-method sudo -m raw -a "sudo pacman --noconfirm -Syu python"
