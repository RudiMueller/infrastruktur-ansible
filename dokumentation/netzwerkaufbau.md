# Aufbau des Netzwerkes

Dieser Teil beschreibt wie die Geräte in meinem Netzwerk miteinander agieren und wo welche Aufgabe erledigt wird.

## Netzwerke

Das Netzwerk ist in mehrere logische Netzwerke geteilt. Dies soll zum einen die Teilnehmer der anderen Netze schützen, zum anderen aber auch Optimierungen ermöglichen.

| Name | VLAN Tag | Farbe | Subnetz | Aufgabe |
| ---- | -------- | ----- | ------- | ------- |
| Internet | - | Rot | 0.0.0.0 | Netz ausserhalb meiner Zuständigkeit. Es kann sich hier auch um ein Zwischennetz zwischen Router/Modem/Endpoint des ISP und meinem Router handeln.|
| Untagged | - | Grau/Weis | 192.168.23.0 | Dieses Netz wird von Geräten benutzt, die in mehreren VLANs aktiv sind. So z.B. Accesspoints die den verbundenen Geräten VLAN Tags zuweisen. |
| Privat | 10 | Blau | 192.168.11.0 | Internes Netz in dem alle meine privaten Geräte, inklusive Server und Mobil-Geräte, eingebettet sind. |
| Gast | 20 | Grün | 192.168.19.0 | Netz für Gäste, die Internetzugang möchten. Auch geschäftliche Geräte sind Gäste in meinem Netzwerk. |
| Automatisierung | 30 | Gelb | 192.168.17.0 | Netz für Heimautomatisierungsgeräte. Dieses Netz soll private Geräte von möglicherweise fehlerhaften Geräten abschirmen. Ausserdem ist Internetzugang nur in Ausnahmefällen erlaubt. |
| Verwaltung | 40 | Schwarz | 192.168.42.0 | In diesem Netz sind die IGMI Schnittstellen der Server, Login-Seiten der Router und Switches sowie die DHCP-, DNS-, NTP- und Authentifizierungs-Server angsiedelt. |

## Geräte

Für die Verwaltung und den Betrieb des Netzwerks sind diverse Geräte im Einsatz. In der folgenden Tabelle sind alle physikalischen Geräte aufgelistet die das Netz betreiben. Als Namen sind die Host-Namen angegeben. Diese werden um die Domäne erweitert für die FQDN.

| Name | IP | Aufgabe |
| ---- | --- | ------- |
| tor | 192.168.xx.1 | Gateway basierend auf PfSense oder OpenSense welches alle Netzwerke mit dem Internet verbindet. |
| weiche | 192.168.xx.2 | Gateway zwischen den VLAN. |
| direktor | 192.168.42.3 | Raspberry Pi Application-Server mit DHCP, DNS, Radius usw. |
| stellvertreter | 192.168.42.4 | Raspberry Pi Fail-over für den direktor. |
| hauptverteiler | 192.168.42.10 | PoE Switch |
| wohnzimmerverteiler | 192.168.42.11 | (Optionaler) Switch im Wohnzimmer um Kabelbäume zu reduzieren. |
| sender | 192.168.42.xx | Ubiquiti Accesspoint. |
| die-farm | 192.168.42.xx | Application-Server für virtuelle Maschinen. |
| lager | 192.168.42.xx | Speicher-Server mit virtuellen Maschinen und großen Langzeitspeichern. |


## Clients

Als Übersicht werden hier auch die anderen Geräte gelistet.

| Name | IP | Aufgabe |
| ---- | --- | ------ |
| waldlaeufer | 192.168.11.xx | Produktivlaptop |
| tafel | 192.168.11.xx | Tablet für Unterhaltung und Steuerung. |
| pinnwand | 192.168.11.xx | Touchbildschirm mit Wetter, Kalender, Nachrichten usw. |
| kampfstation | 192.168.11.xx | Desktop PC für Spiele und rechenintensive Aufgaben. |
| leuchtkaefer | 192.168.11.xx | Smartphone |
| zeitverschwender | 192.168.11.xx | Mediacenter |

## Grundlegende Konfiguration

### DHCP

Die Switches leiten alle DHCP anfragen mit Hilfe von IP-Helper o.ä. an den `direktor` weiter. Dieser vergibt die IP-Adressen und setzt sich selbst als DNS und NTP Server.
Der Standard-Gateway ist `tor`. Zusätzlich sind statische Routen zu den anderen Subnetzen konfiguriert. Das Gateway für diese Routen ist die `weiche`. 
Als alternative Server für DNS usw. wird `stellvertreter` definiert.

### DNS

Der Knoten `direktor` schlüsselt die Namen auf, wenn er sie selbst kennt. Wenn nicht, dann wird ein offener DNS-Server (z.B. des CCC) kontaktiert. ISP DNS sollte wegen der Zensurgefahr vermieden werden.

### Ubiquity

Der Hostname `unifi` wird vom DNS auf sich selbst gesetzt. Somit bekommt man von DNS-Server `direktor` die IP von `direktor` und von DNS-Server `stellvertreter` die IP von `stellvertreter`. Das ermöglicht ein vollständiges Fail-Over.
Auf den Raspberry Pi Servern läuft jeweils eine Instanz des Ubiquity Controllers.

### NTP

Als NTP-Server im Netzwerk agieren `direktor` und `stellvertreter`. Sie werden im DHCP konfiguriert. Wenn ein Knoten einen NTP ausserhalb meines Netzes anfragt, endet der Request bei `tor`. Dieser Gateway beantwortet die Anfrage mit Daten von `direktor` oder `stellvertreter`.
Die NTP-Server haben mindestens drei Pools konfiguriert um solide Resultate zu liefern.

