# Vagrant installation

# Benötigte Packete
- vagrant
- libvirt
- qemu
- polkit: Verwaltet Zugriffsberechtigung auf Funktionen und Tools; Detailierter als Dateibasiert; Für Sicherheit keine Alternative zu sudo!
- lxqt-policykit-agent: Stellt Authentication für GUIs bereit.
- ebtables: wird für die default-Netzwerke benötigt
- dnsmasq: wird für die default-Netzwerke benötigt
- dmidecode
- nfs-utils: Konfiguriere NFS3 und UDP Unterstützung und starte den NFS-Server

## Gruppen
- virt-manager: Benutzer in dieser Gruppe können die virtuellen Maschinen verwalten
- virt-viewer: Benutzer in dieser Gruppe können den Status der virtuellen Maschinen anschauen.

Hinweis: In Arch-Linux können Benutzer der Gruppe `wheel` nach Authentifizierung virtuelle Maschinen verwalten. 

## Polkit Regeln

Damit die Rechte richtig funktionieren muss folgende Regel angelegt werden:
Datei: /etc/polkit-1/rules.d/10-libvirt.rules
```js
  /* Allow users in virt-manager group to manage libvirt hosts without authentication */
  polkit.addRule(function(action, subject) {
      if (action.id == "org.libvirt.unix.manage" &&
          subject.isInGroup("virt-manager")) {
          return polkit.Result.YES;
      }
  });
  
  /* Allow users in virt-viewer group to read libvirt hosts without authentication */
  polkit.addRule(function(action, subject) {
      if (action.id == "org.libvirt.unix.manage" &&
          subject.isInGroup("virt-viewer")) {
          return polkit.Result.YES;
      }
  });
```
## Netzwerkeinstellungen

Das Netzwerk wird als Bridge (Brücke) konfiguriert. Die virtuellen Maschinen haben so volle Kontrolle über ihre äusere Schnittstelle.
Für die Erstellung einer neuen "br0" Brücke sollte die Arch-Wiki konsultiert werden: [Network-Bridge](https://wiki.archlinux.org/index.php/Network_bridge)

# Plugins

- vagrant-libvirt


# Init und Start

``` shell
  vagrant init archlinux/archlinux
  vagrant up --provider=libvirt
```
